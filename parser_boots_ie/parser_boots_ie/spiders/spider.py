# coding: utf8
import scrapy
import re
import json
import csv
import sys
from parser_boots_ie.items import BootsItem

class MacysSpider(scrapy.Spider):
    name = "boots"


    def start_requests(self):
        url = 'http://www.boots.ie/'
        yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@id='topLevelMenu_1596012']//ul[@class='categoryList']/li/a[@class='viewAllHeader']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse1)


    def parse1(self, response):
        urls = response.xpath(".//div[@class='category-link']/a/@href").extract()
        if len(urls)>0:
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)
        else:

            categoryId = response.xpath(".//meta[@name='cmcategoryId']/@content").extract_first()
            itemsCount = response.xpath(".//span[@class='showing_products_total']/text()").extract_first(default=0)
            if itemsCount  != 0:
                # cat = response.xpath(".//li[@class='current']/text()").extract_first().replace('"', '&#34;').replace(
                #     ',', '&#44;')
                for offset in range(0,int(itemsCount),180):
                    url = 'http://www.boots.ie/ProductListingView?searchType=1000&resultsPerPage=180&categoryId='+str(categoryId)+'&beginIndex='+str(offset)
                    yield scrapy.Request(url=response.urljoin(url), callback=self.parse3, headers={'X-Requested-With':'XMLHttpRequest',
                                                                                                   'Content-Type':'application/x-www-form-urlencoded',
                                                                                                   'ADRUM':'isAjax:true'})

    def parse3(self, response):
        urls = response.xpath(".//div[@class='product_name']/a/@href").extract()

        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse6)

    def parse6(self, response):
        product = BootsItem()
        product['url'] = response.url
        img = response.xpath(".//input[@id='s7viewerAsset']/@value").extract_first(default='')
        if img != '':
            product['id'] = img.split('/')[1]
            product['img'] = 'https://boots.scene7.com/is/image/'+img
            product['name'] = response.xpath(".//h1[@itemprop='name']/text()").extract_first(default='').replace('"','&#34;').replace(',','&#44;')
            product['price'] = int(float(response.xpath(".//div[@class='price']/text()").extract_first().replace('"','&#34;').replace(',','&#44;').replace(u'€',''))*100)
            # product['category'] = response.meta['cat']
            yield product


















